# Multi Environment Cluster

Create multi-environment clusters using ASM with uniform naming scheme.

## Preparing the environment.

1.  Clone this repo.

    ```bash
    mkdir -p asm-multienvironment && cd asm-multienvironment && export WORKDIR=`pwd`
    git clone https://gitlab.com/asm7/multi-environment-cluster.git asm-multienv
    ```

1.  Create a project or use an existing project with required APIs enabled. Ensure you are Project Owner rights on the project.

    ```bash
    # Define vars
    export PROJECT_NAME=asm-multi-environment-cluster
    export ORG_ID=<Organization ID>
    export BILLING_ACCOUNT=<Billing Account Number>
    export GKE1=asm-gke
    export GKE1_ZONE=us-central1-a

    # Create project
    gcloud projects create ${PROJECT_NAME} --organization=${ORG_ID}
    export PROJECT_ID=$(gcloud projects list | grep ${PROJECT_NAME} | awk '{ print $2 }')
    gcloud config set project ${PROJECT_ID}

    # Get project number for mesh_id label
    export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format='value(projectNumber)')

    # Enable billing and enable GKE API
    gcloud beta billing projects link ${PROJECT_ID} \
    --billing-account ${BILLING_ACCOUNT}
    gcloud services enable \
    --project=${PROJECT_ID} \
    container.googleapis.com \
    compute.googleapis.com \
    monitoring.googleapis.com \
    logging.googleapis.com \
    cloudtrace.googleapis.com \
    meshca.googleapis.com \
    meshtelemetry.googleapis.com \
    meshconfig.googleapis.com \
    iamcredentials.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    stackdriver.googleapis.com
    ```

## Creating a cluster and deploying ASM

1.  Create a GKE cluster.

    ```bash
    gcloud container clusters create ${GKE1} \
    --project ${PROJECT_ID} \
    --zone=${GKE1_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "2" --min-nodes "2" --max-nodes "4" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --workload-pool=${PROJECT_ID}.svc.id.goog \
    --labels=mesh_id=proj-${PROJECT_NUMBER}
    ```

1.  Get cluster credentials.

    ```bash
    touch asm-kubeconfig
    export KUBECONFIG=`pwd`/asm-kubeconfig
    gcloud container clusters get-credentials ${GKE1} --zone ${GKE1_ZONE} --project ${PROJECT_ID}
    ```

1.  Download the `install_asm` script.

    ```bash
    # Download script
    curl https://storage.googleapis.com/csm-artifacts/asm/install_asm_1.9 > install_asm
    chmod +x install_asm
    mkdir asm-dir

    # Install ASM
    ./install_asm \
    --project_id ${PROJECT_ID} \
    --cluster_name ${GKE1} \
    --cluster_location ${GKE1_ZONE} \
    --mode install \
    --enable_all \
    --output_dir asm-dir \
    --option envoy-access-log
    ```

> ASM takes a few minutes to install. you should see a `Successfully installed ASM` message.

1.  As part of the installation `istioctl` CLI utility is downloaded to your `output_dir` folder.

    ```bash
    export ISTIOCTL_CMD="${WORKDIR}/asm-dir/istioctl"
    ```

1.  Get the ASM label for sidecar injection. Get the value of the `istio.io/rev` label.

    ```bash
    export ASM_LABEL=$(kubectl -n istio-system get pods -l app=istiod -ojson | jq -r '.items[0].metadata.labels."istio.io/rev"')
    echo ${ASM_LABEL}
    ```

## Creating namespaces and deploying Services

1.  Create four namespaces.

    ```bash
    # dev namespaces
    cd ${WORKDIR}/asm-multienv/dev/namespaces
    kustomize edit add label istio.io/rev:"${ASM_LABEL}"
    kubectl apply -k .
    # stage namespaces
    cd ${WORKDIR}/asm-multienv/stage/namespaces
    kustomize edit add label istio.io/rev:"${ASM_LABEL}"
    kubectl apply -k .
    ```

1.  Create `nginx` Deployments in all namespaces.

    ```bash
    kubectl -n dev-frontend apply -f ${WORKDIR}/asm-multienv/dev/deployments/deployment--nginx-dev-frontend.yaml
    kubectl -n dev-backend apply -f ${WORKDIR}/asm-multienv/dev/deployments/deployment--nginx-dev-backend.yaml
    kubectl -n stage-frontend apply -f ${WORKDIR}/asm-multienv/stage/deployments/deployment--nginx-stage-frontend.yaml
    kubectl -n stage-backend apply -f ${WORKDIR}/asm-multienv/stage/deployments/deployment--nginx-stage-backend.yaml
    ```

1.  Create `frontend` and `backend` Services respectively in the `dev-` and `stage-` namespaces.

    ```bash
    kubectl -n dev-frontend apply -f ${WORKDIR}/asm-multienv/dev/services/service--dev-frontend.yaml
    kubectl -n dev-backend apply -f ${WORKDIR}/asm-multienv/dev/services/service--dev-backend.yaml
    kubectl -n stage-frontend apply -f ${WORKDIR}/asm-multienv/stage/services/service--stage-frontend.yaml
    kubectl -n stage-backend apply -f ${WORKDIR}/asm-multienv/stage/services/service--stage-backend.yaml
    ```

1.  Check Deployments and Services in all namespaces.

    ```bash
    kubectl -n dev-frontend get deployments
    kubectl -n dev-frontend get services

    kubectl -n dev-backend get deployments
    kubectl -n dev-backend get services

    kubectl -n stage-frontend get deployments
    kubectl -n stage-frontend get services

    kubectl -n stage-backend get deployments
    kubectl -n stage-backend get services
    ```

Output from each namespace looks like the following:

    kubectl -n <namespace> get deployments
    NAME      READY   UP-TO-DATE   AVAILABLE   AGE
    backend   1/1     1            1           31m
    kubectl -n <namespace> get services
    NAME       TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
    backend    ClusterIP   10.48.7.127   <none>        80/TCP    31m

1.  Ensure all Deployments are Ready and get Pod name.

    ```bash
    # Ensure all deployments are available
    kubectl -n dev-frontend wait --for=condition=available deployment frontend
    kubectl -n dev-backend wait --for=condition=available deployment backend
    kubectl -n stage-frontend wait --for=condition=available deployment frontend
    kubectl -n stage-backend wait --for=condition=available deployment backend

    # Get pods
    export DEV_FRONTEND_POD=$(kubectl -n dev-frontend get pod -l "app=dev-frontend" -o jsonpath='{.items[0].metadata.name}')
    export DEV_BACKEND_POD=$(kubectl -n dev-backend get pod -l "app=dev-backend" -o jsonpath='{.items[0].metadata.name}')
    export STAGE_FRONTEND_POD=$(kubectl -n stage-frontend get pod -l "app=stage-frontend" -o jsonpath='{.items[0].metadata.name}')
    export STAGE_BACKEND_POD=$(kubectl -n stage-backend get pod -l "app=stage-backend" -o jsonpath='{.items[0].metadata.name}')
    ```

1.  Ensure pods respond with proper values.

    ```bash
    kubectl -n dev-frontend exec ${DEV_FRONTEND_POD} -c nginx -- curl -s localhost
    # Output should be dev-frontend
    kubectl -n dev-backend exec ${DEV_BACKEND_POD} -c nginx -- curl -s localhost
    # Output should be dev-backend
    kubectl -n stage-frontend exec ${STAGE_FRONTEND_POD} -c nginx -- curl -s localhost
    # Output should be stage-frontend
    kubectl -n stage-backend exec ${STAGE_BACKEND_POD} -c nginx -- curl -s localhost
    # Output should be stage-backend
    ```

1.  Without ASM, you can access to Services using `svc_name.namespace`.

    ```bash
    kubectl -n dev-frontend exec -t ${DEV_FRONTEND_POD} -c nginx -- curl -s backend.dev-backend
    kubectl -n dev-frontend exec -t ${DEV_FRONTEND_POD} -c nginx -- curl -s backend.stage-backend
    kubectl -n stage-frontend exec -t ${STAGE_FRONTEND_POD} -c nginx -- curl -s backend.dev-backend
    kubectl -n stage-frontend exec -t ${STAGE_FRONTEND_POD} -c nginx -- curl -s backend.stage-backend
    ```

## Inspecting ASM configs

1.  ```bash
    $ISTIOCTL_CMD -n dev-frontend proxy-config route ${DEV_FRONTEND_POD}
    ```

Output looks like the following:

    NAME             DOMAINS                               MATCH                  VIRTUAL SERVICE
    80               backend.dev-backend                   /*
    80               backend.stage-backend                 /*
    80               default-http-backend.kube-system      /*
    80               frontend                              /*
    80               frontend.stage-frontend               /*
    80               istio-ingressgateway.istio-system     /*
    15010            istiod-asm-191-1.istio-system         /*
    15010            istiod.istio-system                   /*
    15014            istiod-asm-191-1.istio-system         /*
    15014            istiod.istio-system                   /*
    inbound|80||     *                                     /*
                     *                                     /stats/prometheus*
    inbound|80||     *                                     /*
                     *                                     /healthz/ready*

    ```bash
    $ISTIOCTL_CMD -n dev-frontend proxy-config routes ${DEV_FRONTEND_POD} -ojson | jq '(.[0].virtualHosts[] | select(.name=="backend.dev-backend.svc.cluster.local:80"))'
    ```

Output looks like the following:

    "name": "backend.dev-backend.svc.cluster.local:80",
    "domains": [
      "backend.dev-backend.svc.cluster.local",
      "backend.dev-backend.svc.cluster.local:80",
      "backend.dev-backend",
      "backend.dev-backend:80",
      "backend.dev-backend.svc.cluster",
      "backend.dev-backend.svc.cluster:80",
      "backend.dev-backend.svc",
      "backend.dev-backend.svc:80",
      "10.48.1.148",
      "10.48.1.148:80"
    ],
    "routes": [
      {
        "name": "default",
        "match": {
          "prefix": "/"
        },
        "route": {
          "cluster": "outbound|80||backend.dev-backend.svc.cluster.local"



    ```bash
    $ISTIOCTL_CMD -n dev-frontend proxy-config endpoints ${DEV_FRONTEND_POD} | grep -e stage -e dev
    ```

Output looks like the following:

    10.44.0.10:80                    HEALTHY     OK                outbound|80||backend.stage-backend.svc.cluster.local
    10.44.0.11:80                    HEALTHY     OK                outbound|80||frontend.dev-frontend.svc.cluster.local
    10.44.0.8:80                     HEALTHY     OK                outbound|80||frontend.stage-frontend.svc.cluster.local
    10.44.0.9:80                     HEALTHY     OK                outbound|80||backend.dev-backend.svc.cluster.local

## Configuring ASM

Create normalized naming using ASM. You can use ASM so that when `frontend` Service in the `dev-` namespace calls `backend`, it resolves to the `backend.dev-backend` Service. Likewise, when `frontend` Service in the `stage-` namespace calls `backend`, it resolves to the `backend-stage-backend` Service.

1.  Create a Kubernetes Service called `backend` in the `dev-frontend` namespace. ASM requires that the Pods can resolve a hostname. In order to resolve the hostname `backend` in the `dev-frontend` namespace, you can simply create a Kubernetes Service called `backend`. You can use other methods for DNS resolution, for example deploying your own DNS servers and ensuring that the Pods can correctly resolve hostnames. Creating a Service creates a DNS entry in kube-dns which all Pods can use.

    ```bash
    kubectl -n dev-frontend apply -f ${WORKDIR}/asm-multienv/dev/services/service--dev-backend.yaml
    ```

1.  Create a VirtualService in the `dev-frontend` namespace to route Host `backend` to `backend.dev-backend` Service.

    ```bash
    kubectl -n dev-frontend apply -f ${WORKDIR}/asm-multienv/dev/asm/virtualservice--backend-in-dev-frontend.yaml
    ```

    ```bash
    $ISTIOCTL_CMD -n dev-frontend proxy-config routes ${DEV_FRONTEND_POD} -ojson | jq '(.[0].virtualHosts[] | select(.name=="backend.dev-frontend.svc.cluster.local:80"))'
    ```

Output looks like the following:

    "name": "backend.dev-frontend.svc.cluster.local:80",
    "domains": [
      "backend.dev-frontend.svc.cluster.local",
      "backend.dev-frontend.svc.cluster.local:80",
      "backend",
      "backend:80",
      "backend.dev-frontend.svc.cluster",
      "backend.dev-frontend.svc.cluster:80",
      "backend.dev-frontend.svc",
      "backend.dev-frontend.svc:80",
      "backend.dev-frontend",
      "backend.dev-frontend:80",
      "10.48.13.121",
      "10.48.13.121:80"
    ],
    "routes": [
      {
        "match": {
          "prefix": "/"
        },
        "route": {
          "cluster": "outbound|80||backend.dev-backend.svc.cluster.local",

1.  Curl from `frontend.dev-frontend` Pod to `backend`.

    ```bash
    kubectl -n dev-frontend exec -t ${DEV_FRONTEND_POD} -c nginx -- curl -s backend
    ```

Do the same for the `stage-` environment.

1.  Create a `backend` Service in the `stage-frontend` namespace.

    ```bash
    kubectl -n stage-frontend apply -f ${WORKDIR}/asm-multienv/stage/services/service--stage-backend.yaml
    ```

1.  Create a VirtualService in the `stage-frontend` namespace to route Host `backend` to `backend.stage-backend` Service.

    ```bash
    kubectl -n stage-frontend apply -f ${WORKDIR}/asm-multienv/stage/asm/virtualservice--backend-in-stage-frontend.yaml
    ```

1.  Curl from `frontend.stage-frontend` Pod to `backend`.

    ```bash
    kubectl -n stage-frontend exec -t ${STAGE_FRONTEND_POD} -c nginx -- curl -s backend
    ```

1.  Check the `frontend.dev-frontend` Pods proxy config clusters.

    ```bash
    $ISTIOCTL_CMD -n dev-frontend proxy-config clusters ${DEV_FRONTEND_POD} --direction outbound
    ```

Output looks like the following:

    backend.dev-backend.svc.cluster.local                                                 80        -          outbound      EDS
    backend.dev-frontend.svc.cluster.local                                                80        -          outbound      EDS
    backend.stage-backend.svc.cluster.local                                               80        -          outbound      EDS
    backend.stage-frontend.svc.cluster.local                                              80        -          outbound      EDS

1.  Apply the sidecar resource.

    ```bash
    kubectl -n dev-frontend apply -f ${WORKDIR}/asm-multienv/dev/asm/sidecar--dev-frontend.yaml
    ```

1.  Check the proxy config clusters again.

    ```bash
    $ISTIOCTL_CMD -n dev-frontend proxy-config clusters ${DEV_FRONTEND_POD} --direction outbound
    ```

Output looks like the following:

    SERVICE FQDN                                PORT     SUBSET     DIRECTION     TYPE     DESTINATION RULE
    backend.dev-backend.svc.cluster.local       80       -          outbound      EDS
    backend.dev-frontend.svc.cluster.local      80       -          outbound      EDS
    frontend.dev-frontend.svc.cluster.local     80       -          outbound      EDS

1.  Apply the sidecar configs to the `stage-frontend` namespace.

    ```bash
    kubectl -n stage-frontend apply -f ${WORKDIR}/asm-multienv/stage/asm/sidecar--stage-frontend.yaml
    ```

1.  Check the proxy config clusters again.

    ```bash
    $ISTIOCTL_CMD -n stage-frontend proxy-config clusters ${STAGE_FRONTEND_POD} --direction outbound
    ```

Output looks like the following:

    SERVICE FQDN                                  PORT     SUBSET     DIRECTION     TYPE     DESTINATION RULE
    backend.stage-backend.svc.cluster.local       80       -          outbound      EDS
    backend.stage-frontend.svc.cluster.local      80       -          outbound      EDS
    frontend.stage-frontend.svc.cluster.local     80       -          outbound      EDS
